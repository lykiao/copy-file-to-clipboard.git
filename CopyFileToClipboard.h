#define EXPORT __declspec(dllexport)
using namespace std;

extern "C" {
	EXPORT int RunCopyFile(const char *charPath);
	EXPORT int CopyFileToClipboard(char szFileName[]);
	EXPORT char *utf8ToGBK(char *strUTF);
}