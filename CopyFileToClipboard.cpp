// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <iostream>
#include <windows.h>
#include <shlobj.h>
#include "CopyFileToClipboard.h"
using namespace std;
int RunCopyFile(const char *charPath) {
	char *buf = new char[strlen(charPath) + 1];
	strcpy(buf, charPath);
	char* strGBK = utf8ToGBK(buf);
	int result = CopyFileToClipboard(strGBK);
	return result;
}

//solve Chinese character code
//首先把UTF-8转换成Unicode编码，而后把unicode转换成系统编码。
char *utf8ToGBK(char *strUTF) {
	int size;
	size = MultiByteToWideChar(CP_UTF8, 0, strUTF, -1, NULL, 0);
	wchar_t* strUnicode = new wchar_t[size + 1];
	MultiByteToWideChar(CP_UTF8, 0, strUTF, -1, strUnicode, size);
	size = WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, NULL, 0, NULL, NULL);
	char* strGBK = new char[size + 1];
	memset(strGBK, 0, size + 1);
	WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, strGBK, size, NULL, NULL);
	delete[]strUnicode;
	return strGBK;
}

int CopyFileToClipboard(char szFileName[])
{
	UINT uDropEffect;
	HGLOBAL hGblEffect;
	LPDWORD lpdDropEffect;
	DROPFILES stDrop;

	HGLOBAL hGblFiles;
	LPSTR lpData;

	uDropEffect = RegisterClipboardFormat("Preferred DropEffect");
	hGblEffect = GlobalAlloc(GMEM_ZEROINIT | GMEM_MOVEABLE | GMEM_DDESHARE, sizeof(DWORD));
	lpdDropEffect = (LPDWORD)GlobalLock(hGblEffect);
	*lpdDropEffect = DROPEFFECT_COPY;//复制; 剪贴则du用DROPEFFECT_MOVE
	GlobalUnlock(hGblEffect);

	stDrop.pFiles = sizeof(DROPFILES);
	stDrop.pt.x = 0;
	stDrop.pt.y = 0;
	stDrop.fNC = FALSE;
	stDrop.fWide = FALSE;

	hGblFiles = GlobalAlloc(GMEM_ZEROINIT | GMEM_MOVEABLE | GMEM_DDESHARE, \
		sizeof(DROPFILES) + strlen(szFileName) + 2);
	lpData = (LPSTR)GlobalLock(hGblFiles);
	memcpy(lpData, &stDrop, sizeof(DROPFILES));
	strcpy(lpData + sizeof(DROPFILES), szFileName);
	GlobalUnlock(hGblFiles);

	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_HDROP, hGblFiles);
	SetClipboardData(uDropEffect, hGblEffect);
	CloseClipboard();

	return 1;
}
